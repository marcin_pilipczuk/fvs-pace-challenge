#!/bin/bash

DIAG=./release/fvs_diagnose
testdir="pace16-fvs-instances"
timetmp=`mktemp`

for i in `ls $testdir/*.graph`
do
  echo "================================"
  echo "Test $i:"
  echo ""
  /usr/bin/time -q --output=$timetmp -f "%E" $DIAG -f $i 2>&1
  echo -n "TOTAL TIME SPENT: "
  cat $timetmp
  echo ""
done
