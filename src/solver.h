/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FVS_PACE_CHALLENGE_SOLVER_H
#define FVS_PACE_CHALLENGE_SOLVER_H

#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <deque>
using namespace std;

#include "graph.h"

class solver{
protected:
    vector<int> best_solution;
    int solution_size_cap = INF;
    vector<string> vertex_names;
    bool solution_found = false;

    bool reductions(graph &g);
    void branching(graph &g);
    void iterative_compression(graph &g, vector<int> &apx_sol);
    void short_iterative_compression(graph &g, vector<int> &apx_sol);
    vector<int> greedy_approximation(graph g);

public:
    void set_vertex_names(vector<string> &vertex_names);
    void print_solution();
    void solve(graph &g);
    vector<int> approximate(graph &g);
    void set_approximate_solution_size_cap(graph &g);
};


#endif //FVS_PACE_CHALLENGE_SOLVER_H
