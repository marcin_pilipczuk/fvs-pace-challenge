/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include "deg3solver.h"
#include "lowdegsolver.h"

bool lowdegsolver::reductions(graph &g) {
    /* Internal simplification rules for graph */
    g.simplify();
    /* If multiple connected components, split along them */
    int large = g.components();
    for (int i = 0; i < (int) g.ccomps.size(); ++i)
        if (i != large) {
            graph gc = graph(g, g.ccomps[i], g.cc_entry[i]);
            lowdegsolver s;
            s.solution_size_cap = solution_size_cap - (int)g.solution.size();
            if (g.cc_entry[i] < 0)
                s.branching(gc);
            else {
                assert(!gc.branching_hints.empty());
                int v = gc.branching_hints.front();
                gc.branching_hints.pop_front();
                graph gc2 = gc;
                gc.proclaim_solution(v);
                s.branching(gc);
                gc2.proclaim_undeletable(v);
                s.branching(gc2);
            }
            if (s.solution_found)
                g.proclaim_solution(s.best_solution);
            else
                return true;
        }
    g.ccomps.clear(); g.cc_entry.clear();
    g.simplify();
    /* If cubic, invoke poly-time solver */
    if (g.max_degree() == 3){
        deg3solver deg3s(g);
        vector<int> ret = deg3s.solve();
        g.proclaim_solution(ret);
        g.simplify();
    }
    return false;
}

void lowdegsolver::solve(graph &g) {
    g.simplify();
    vector<int> apx = approximate(g);
    g.branching_hints = deque<int>(apx.begin() + g.solution.size(), apx.end());
    branching(g);
}

void lowdegsolver::branching(graph &g){
    if (!reductions(g) && (int)g.solution.size() < solution_size_cap){
        int v = g.choose_branching_vertex_IC();
        if (v == -1){
            // instance already solved
            solution_found = true;
            best_solution = g.give_solution();
            solution_size_cap = (int)best_solution.size();
        } else {
            graph second_copy = g;
            g.proclaim_solution(v);
            branching(g);
            second_copy.proclaim_undeletable(v);
            branching(second_copy);
        }
    }
}
