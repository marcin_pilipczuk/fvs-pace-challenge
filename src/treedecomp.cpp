/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include "treedecomp.h"

treedecomp::treedecomp(graph &_g) : g(_g), active((unsigned long)_g.n, 0), comps((unsigned long)_g.n, 1), last_comp_id(1) {
    td_comp.push_back(vector<int>());
    td_bag.push_back(vector<int>());
    td_border.push_back(vector<int>());
    td_children.push_back(vector<int>());
    td_comp.push_back(g.vertex_list);
    td_bag.push_back(vector<int>());
    td_border.push_back(vector<int>());
    td_children.push_back(vector<int>());

    // Initiate abandon threshold
    TD_ABANDON_THRESHOLD = TD_BASE_ABANDON_THRESHOLD;
    int n = g.n;
    while (n >= 256) {
        n >>= 1;
        TD_ABANDON_THRESHOLD -= 2;
    }
}

class TdThresholdExceeded {};

bool treedecomp::dfs_mincut(int v) {
    vis[v] = iter_id;
    if (out[v] == INF) // Hurray, sink!
        return true;
    // Try to go backwards along the incoming flow edge
    if (in[v] >= 0 && in[v] != INF && vis[in[v]] < iter_id && dfs_mincut(in[v])) {
        in[v] = out[v] = -1;
        return true;
    }
    // Scan outgoing edges
    for (int i = 0; i < (int)g.nei[v].size(); ++i){
        int w = g.nei[v][i];
        // Do not go over not active edges, to sources, and along the passing flow path
        if (active[w] + active[v] < 3 || in[w] == INF || w == in[v] || w == out[v]) continue;
        int step = in[w] >= 0 ? in[w] : w;
        if (vis[step] < iter_id && dfs_mincut(step)){
            out[v] = w;
            in[w] = v;
            return true;
        }
    }
    return false;
}

vector<int> treedecomp::mincut(vector<int> &s, vector<int> &t) {
    vis = vector<int>((unsigned long)g.n, 0);
    in = vector<int>((unsigned long)g.n, -1);
    out = vector<int>((unsigned long)g.n, -1);
    iter_id = 0;

    for (__typeof(s.begin()) it = s.begin(); it != s.end(); ++it) {
        assert(active[*it] == 1);
        in[*it] = INF;
    }
    for (__typeof(t.begin()) it = t.begin(); it != t.end(); ++it) {
        assert(active[*it] == 1);
        out[*it] = INF;
    }
    bool path_found;
    int flowsize = 0;
    do{
        path_found = false;
        ++iter_id;
        for (__typeof(s.begin()) it = s.begin(); it != s.end(); ++it)
            if (out[*it] == -1 && vis[*it] < iter_id)
                if (dfs_mincut(*it)) {
                    path_found = true;
                    flowsize++;
                    break;
                }
    }while(path_found);
    vector<int> ret((unsigned long)g.n, 0), cut;
    for (__typeof(s.begin()) it = s.begin(); it != s.end(); ++it)
        if (vis[*it] < iter_id) {
            ret[*it] = 1;
            cut.push_back(*it);
            if (out[*it] == INF)
                flowsize++;
        }
    for (__typeof(g.vertex_list.begin()) it = g.vertex_list.begin(); it != g.vertex_list.end(); ++it)
        if (active[*it] && vis[*it] == iter_id)
            for (int i = 0; i < (int)g.nei[*it].size(); ++i) {
                int w = g.nei[*it][i];
                if (active[*it] + active[w] >= 3 && vis[w] < iter_id) {
                    if (ret[w] == 0)
                        cut.push_back(w);
                    ret[w] = 1;
                }
            }
    assert(flowsize == (int)cut.size());
    return cut;
}

void treedecomp::find_best_separator(vector<int> &border, int pos, vector<int> &s, vector<int> &t) {
    if (pos == (int)border.size()){
        int m = min((int)s.size(), (int)t.size());
        if (m == 0) return;
        vector<int> cut = mincut(s, t);
        int mc = (int)cut.size();
        if (m - mc > best_cut_score){
            best_cut_score = m - mc;
            best_cut = cut;
        }
    } else {
        int v = border[pos++];
        s.push_back(v);
        find_best_separator(border, pos, s, t);
        s.pop_back();
        t.push_back(v);
        find_best_separator(border, pos, s, t);
        t.pop_back();
    }
}

int treedecomp::__choose_extension_vertex_arbitrarily(int comp_id) {
    for (int i = 0; i < g.n; ++i)
        if (comps[i] == comp_id)
            return i;
    assert(false);
    return -1;
}

int treedecomp::__choose_extension_vertex_max_border_deg(int comp_id) {
    int best_deg = -1, best_v = -1;
    for (int v = 0; v < g.n; ++v)
        if (comps[v] == comp_id) {
            int deg = 0;
            for (int i = 0; i < (int) g.nei[v].size(); ++i)
                if (!comps[g.nei[v][i]])
                    ++deg;
            if (deg > best_deg) {
                best_deg = deg;
                best_v = v;
            }
        }
    assert(best_v >= 0);
    return best_v;
}

int treedecomp::__choose_extension_vertex_border_replace(int comp_id, vector<int> &border) {
    for (__typeof(border.begin()) it = border.begin(); it != border.end(); ++it){
        int unei = -1;
        for (int i = 0; i < (int) g.nei[*it].size(); ++i)
            if (comps[g.nei[*it][i]] == comp_id) {
                if (unei == -1)
                    unei = g.nei[*it][i];
                else {
                    unei = -1;
                    break;
                }
            }
        if (unei >= 0)
            return unei;
    }
    return __choose_extension_vertex_max_border_deg(comp_id);
}

int treedecomp::choose_extension_vertex(int comp_id, vector<int> &border) {
    return __choose_extension_vertex_border_replace(comp_id, border);
}

vector<int> treedecomp::nextbag(int comp_id) {
    vector<int> border;
    best_cut.clear();
    best_cut_score = 0;
    for (__typeof(g.vertex_list.begin()) it = g.vertex_list.begin(); it != g.vertex_list.end(); ++it)
        active[*it] = 0;
    for (__typeof(td_comp[comp_id].begin()) it = td_comp[comp_id].begin(); it != td_comp[comp_id].end(); ++it) {
        active[*it] = 2;
        for (int i = 0; i < (int) g.nei[*it].size(); ++i)
            if (comps[g.nei[*it][i]] != comp_id)
                active[g.nei[*it][i]] = 1;
    }
    for (__typeof(g.vertex_list.begin()) it = g.vertex_list.begin(); it != g.vertex_list.end(); ++it)
        if (active[*it] == 1) {
            assert(comps[*it] == 0);
            border.push_back(*it);
        }
    assert((int)border.size() <= TD_ABANDON_THRESHOLD);
    td_border[comp_id] = border;
    vector<int> s, t;
    find_best_separator(border, 0, s, t);
    if (best_cut_score > 0){
        for (__typeof(best_cut.begin()) it = best_cut.begin(); it != best_cut.end(); ++it)
            if (comps[*it] == comp_id)
                border.push_back(*it);
    } else {
        // expand by one
        if ((int)border.size() == TD_ABANDON_THRESHOLD)
            throw TdThresholdExceeded();
        int new_vertex = choose_extension_vertex(comp_id, border);
        assert(new_vertex >= 0 && comps[new_vertex] == comp_id);
        border.push_back(new_vertex);
    }
    td_bag[comp_id] = border;
    return border;
}

void treedecomp::dfs_flood_comp(int v) {
    td_comp[last_comp_id].push_back(v);
    comps[v] = last_comp_id;
    for (int i = 0; i < (int)g.nei[v].size(); ++i) {
        int w = g.nei[v][i];
        if (comps[w] > 0 && comps[w] != last_comp_id)
            dfs_flood_comp(w);
    }
}

void treedecomp::splitcomp(int comp_id, vector<int> &bag) {
    for (__typeof(bag.begin()) it = bag.begin(); it != bag.end(); ++it)
        if (comps[*it] == comp_id)
            comps[*it] = 0;
    for (__typeof(g.vertex_list.begin()) it = g.vertex_list.begin(); it != g.vertex_list.end(); ++it)
        if (comps[*it] == comp_id) {
            ++last_comp_id;
            td_children[comp_id].push_back(last_comp_id);
            td_comp.push_back(vector<int>());
            td_bag.push_back(vector<int>());
            td_border.push_back(vector<int>());
            td_children.push_back(vector<int>());
            dfs_flood_comp(*it);
        }
}

int treedecomp::approximate_width() {
    int curr_comp = 1;
    int width = 0;
    vector<int> bag;
    while (curr_comp <= last_comp_id){
        try {
            bag = nextbag(curr_comp);
        } catch(const TdThresholdExceeded &e) {
            return -1;
        }
        width = max(width, (int)bag.size());
#ifdef TD_DEBUG_PRINT_BAGS
        fprintf(stderr, "  BAG(%d):", (int)bag.size());
        for (__typeof(bag.begin()) it = bag.begin(); it != bag.end(); ++it)
            fprintf(stderr, " %s", vertex_names[*it].c_str());
        fprintf(stderr, "\n");
#endif
        splitcomp(curr_comp, bag);
        curr_comp++;
    }
    return width; // f*** minus one.
}

void treedecomp::set_vertex_names(vector<string> &vn) {
    vertex_names = vn;
}

/******************* DP **************************/

dp_state::dp_state() {
#ifdef TD_COMPRESS_STATES
    for (int i = 0; i < TD_64COUNT; ++i)
        t[i] = 0;
#else
#endif
}
uint64_t dp_state::get_leader(uint64_t a) const {
#ifdef TD_COMPRESS_STATES
    int p = 0;
    while (a >= TD_FIELDS_PER_64) {
        a -= TD_FIELDS_PER_64;
        p++;
    }
    return ((t[p] >> (TD_FIELD_SIZE * a)) & TD_FIELD_MASK);
#else
    return (uint64_t)t[a];
#endif
}

void dp_state::set_leader(uint64_t a, uint64_t b) {
#ifdef TD_COMPRESS_STATES
    int p = 0;
    while (a >= TD_FIELDS_PER_64) {
        a -= TD_FIELDS_PER_64;
        p++;
    }
    t[p] &= ~ (TD_FIELD_MASK << (a * TD_FIELD_SIZE));
    t[p] |= ((uint64_t)b) << (a * TD_FIELD_SIZE);
#else
    t[a] = (uint8_t)b;
#endif
}

bool CmpDpState::operator()(const dp_state &a, const dp_state &b) const {
#ifdef TD_COMPRESS_STATES
    for (int i = 0; i < TD_64COUNT; ++i)
        if (a.t[i] != b.t[i])
            return a.t[i] < b.t[i];
    return false;
#else
    for (int i = 0; i < TD_MAX_STATE_SIZE; ++i)
        if (a.t[i] != b.t[i])
            return a.t[i] < b.t[i];
    return false;
#endif
}

static int find_int_on_vector(vector<int> &v, int x){
    for(int i = 0; i < (int)v.size(); ++i)
        if (v[i] == x)
            return i;
    return -1;
}

bool treedecomp::dpstate_merge(dp_state &state, int n, uint64_t a, uint64_t b) {
    a = state.get_leader(a);
    b = state.get_leader(b);
    if (a == b)
        return false;
    if (a > b)
        swap(a, b);
    for (uint64_t v = 0; v < (uint64_t)n; ++v)
        if (state.get_leader(v) == b)
            state.set_leader(v, a);
    return true;
}

bool treedecomp::compute_base_state(vector<int> &verts, uint64_t mask, dp_state &base_state,
                                    vector<int> &filtered_verts) {
    filtered_verts.clear();
    for (int i = 0; i < (int)verts.size(); ++i)
        if (!(mask & (((uint64_t)1) << i))) {
            int v = verts[i];
            uint64_t fi = filtered_verts.size();
            filtered_verts.push_back(v);
            base_state.set_leader(fi, fi);
            for (int j = 0; j < (int)g.nei[v].size(); ++j) {
                int w = g.nei[v][j], p = find_int_on_vector(filtered_verts, w);
                if (p >= 0)
                if (g.nei_double[v][j] || !dpstate_merge(base_state, (int)filtered_verts.size(), (uint64_t)p, fi))
                    return false;
            }
        }
    return true;
}

uint64_t treedecomp::project_mask(vector<int> &verts, uint64_t mask, vector<int> &child_verts) {
    uint64_t ret = 0;
    for (int i = 0; i < (int)child_verts.size(); ++i) {
        int p = find_int_on_vector(verts, child_verts[i]);
        assert(p >= 0);
        ret |= ((mask >> p) & ((uint64_t)1)) << i;
    }
    return ret;
}

bool treedecomp::join_states(dp_state &parent, int n, const dp_state &child, const dp_state &child_base_state, int m,
                             const vector<int> &tsl) {
    for (uint64_t v = 0; v < (uint64_t)m; ++v)
        if (child_base_state.get_leader(v) == v && child.get_leader(v) != v)
            if (!dpstate_merge(parent, n, (uint64_t)tsl[child.get_leader(v)], (uint64_t)tsl[v]))
                return false;
    return true;
}

uint64_t treedecomp::update_dp_table(statearray &SA, const dp_state &state, vector<int> &sol1, vector<int> &sol2) {
    uint64_t mem_usage = 0;
    if (SA.state2index.count(state) == 0 || SA.best_sol[SA.state2index[state]].size() > sol1.size() + sol2.size()) {
        uint64_t nidx;
        if (SA.state2index.count(state) == 0) {
            nidx = SA.states.size();
            SA.states.push_back(state);
            SA.best_sol.push_back(vector<int>());
            SA.state2index[state] = nidx;
            mem_usage = 24 + (sol1.size() + sol2.size()) / 2;
        } else
            nidx = SA.state2index[state];
        SA.best_sol[nidx] = sol1;
        SA.best_sol[nidx].insert(SA.best_sol[nidx].end(), sol2.begin(), sol2.end());
    }
    return mem_usage;
}

vector<int> treedecomp::solve() {
    vector<vector<statearray> > solutions((unsigned long)last_comp_id + 1);
    vector<uint64_t> mem_usage((unsigned long)last_comp_id + 1, 0);
    uint64_t total_mem_usage = 0;
    // Go backwards in the component list
    for (int comp_id = last_comp_id; comp_id >= 1; --comp_id) {
        vector<int> &border = td_border[comp_id];
        vector<int> &bag = td_bag[comp_id];
        unsigned long border_size = border.size(), bag_size = bag.size();
        solutions[comp_id].resize(1UL << border_size);
        // Iterate over every deletion set in border
        for (uint64_t border_mask = 0; border_mask < (1ULL << border_size); ++border_mask) {
            // Shortcut to current statearray we fill in
            dp_state border_base_state;
            statearray &borderSA = solutions[comp_id][border_mask];
            vector<int> filtered_border;
            if (!compute_base_state(border, border_mask, border_base_state, filtered_border))
                continue;
            // Iterate over every extension of the deletion set in the bag
            for (uint64_t bag_mask = border_mask; bag_mask < (((uint64_t)1) << bag_size);
                 bag_mask += ((uint64_t)1) << border_size) {
                dp_state bag_base_state;
                vector<int> filtered_bag;
                if (!compute_base_state(bag, bag_mask, bag_base_state, filtered_bag))
                    continue;
                // Compute newly deleted guys
                vector<int> new_deleted;
                for (uint64_t i = border_size; i < bag_size; ++i)
                    if ((bag_mask >> i) & (uint64_t) 1)
                        new_deleted.push_back(bag[i]);
                // If we have children, we iterate over them, keeping the set of current states
                if (td_children[comp_id].size() > 0) {
                    statearray bagSA;
                    bagSA.states.push_back(bag_base_state);
                    bagSA.best_sol.push_back(vector<int>());
                    bagSA.state2index[bag_base_state] = 0;
                    uint64_t mem_usage_bagSA = 24;
                    // Iterate over every child node
                    for (__typeof(td_children[comp_id].begin()) it = td_children[comp_id].begin();
                         it != td_children[comp_id].end(); ++it) {
                        bool last_child = (*it == td_children[comp_id].back());
                        statearray new_bagSA;
                        uint64_t mem_usage_new_bagSA = 0;
                        dp_state child_base_state;
                        vector<int> &child_border = td_border[*it];
                        vector<int> filtered_child_border;
                        uint64_t child_mask = project_mask(bag, bag_mask, child_border);
                        statearray &childSA = solutions[*it][child_mask];
                        compute_base_state(child_border, child_mask, child_base_state, filtered_child_border);
                        vector<int> tsl(filtered_child_border.size());
                        for (int v = 0; v < (int) filtered_child_border.size(); ++v)
                            tsl[v] = find_int_on_vector(filtered_bag, filtered_child_border[v]);
                        // For every pair of states, try to merge
                        for (uint64_t idx = 0; idx < bagSA.states.size(); ++idx) {
                            for (uint64_t cidx = 0; cidx < childSA.states.size(); ++cidx) {
                                dp_state state = bagSA.states[idx];
                                // Join states
                                if (!join_states(state, (int) filtered_bag.size(), childSA.states[cidx],
                                                 child_base_state,
                                                 (int) filtered_child_border.size(), tsl))
                                    continue;
                                if (!last_child)
                                    mem_usage_new_bagSA += update_dp_table(new_bagSA, state, bagSA.best_sol[idx],
                                                                           childSA.best_sol[cidx]);
                                else {
                                    // Last child: we on the fly project back to the border to save memory
                                    vector<int> sol = new_deleted;
                                    sol.insert(sol.end(), bagSA.best_sol[idx].begin(), bagSA.best_sol[idx].end());
                                    // Project bag solution to border solution
                                    // Luckily, this is easy, because border is a prefix of bag
                                    // and every component's leader is the first guy in this component
                                    for (uint64_t v = filtered_border.size(); v < filtered_bag.size(); ++v)
                                        state.set_leader(v, 0);
                                    uint64_t mem = update_dp_table(borderSA, state, sol, childSA.best_sol[cidx]);
                                    total_mem_usage += mem;
                                    mem_usage[comp_id] += mem;
                                    if (mem_usage_bagSA + total_mem_usage > TD_MEM_LIMIT)
                                        throw TdOutOfMemory();

                                }
                            }
                            if (total_mem_usage + mem_usage_bagSA + mem_usage_new_bagSA > TD_MEM_LIMIT)
                                throw TdOutOfMemory();
                        }
                        swap(new_bagSA.states, bagSA.states);
                        swap(new_bagSA.best_sol, bagSA.best_sol);
                        swap(new_bagSA.state2index, bagSA.state2index);
                        mem_usage_bagSA = mem_usage_new_bagSA;
                    }
                } else {
                    // If we do not have children, we just put base state projected on the border
                    for (uint64_t v = filtered_border.size(); v < filtered_bag.size(); ++v)
                        bag_base_state.set_leader(v, 0);
                    vector<int> dummy;
                    uint64_t mem = update_dp_table(borderSA, bag_base_state, new_deleted, dummy);
                    total_mem_usage += mem;
                    mem_usage[comp_id] += mem;
                    if (total_mem_usage > TD_MEM_LIMIT)
                        throw TdOutOfMemory();

                }
            }
        }
        // The children data is no longer needed
        for (__typeof(td_children[comp_id].begin()) it = td_children[comp_id].begin();
             it != td_children[comp_id].end(); ++it) {
            vector<statearray>().swap(solutions[*it]);
            total_mem_usage -= mem_usage[*it];
        }
    }
    // component number 1 should have one solution, and this is the answer
    assert(solutions[1].size() == 1);
    statearray &SA = solutions[1][0];
    assert(SA.states.size() == 1);
    return SA.best_sol[0];
}
