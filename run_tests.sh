#!/bin/bash

make -C release

### Command line options
# -d test_dir       use tests from test_dir
# -t timeout        set timeout for single test
# -r                write report to a file

exe=./release/fvs_pace_challenge
ver=./release/fvs_verifier
testdir="my-instances"
instancetimeout=1
timeoutset=0
testdirset=0
testbatchset=0
testbatchtimeout=600
testbatch=

mark_test="\e[1m"
mark_ok="\e[32m"
mark_wrong="\e[31m"
mark_tle="\e[35m"
mark_end="\e[0m"

while getopts "d:t:rb:" opt; do
  case $opt in
    d) 
      testdir=$OPTARG
      testdirset=1
      ;;
    t)
      instancetimeout=$OPTARG
      timeoutset=1
      ;;
    r)
      timestamp=`date +"%F-%H%M%S"`
      githash=`git rev-parse HEAD`
      filename="results-$timestamp-$githash.txt"
      exec >$filename
      mark_test=
      mark_ok=
      mark_wrong=
      mark_tle=
      mark_end=
      ;;
    b)
      testbatchset=1
      testbatchname=$OPTARG
      if [ "$testbatchname" = "perf" ]; then
        testbatch="005 009 011 037 044 046 049 092"
        testbatchtimeout=600
      elif [ "$testbatchname" = "perf+" ]; then
        testbatch="005 009 011 037 044 046 049 092 017 076 069 051"
        testbatchtimeout=1200
      elif [ "$testbatchname" = "lowdeg" ]; then
        testbatch="051 060 029 047"
        testbatchtimeout=3600
      elif [ "$testbatchname" = "lowdeg+" ]; then
        testbatch="051 060 029 047 026 061 097"
        testbatchtimeout=3600
      elif [ "$testbatchname" = "deg3" ]; then
        testbatch="012 013 027 033"
        testbatchtimeout=60
      elif [ "$testbatchname" = "deg3+" ]; then
        testbatch="012 013 027 033 084"
        testbatchtimeout=600
      elif [ "$testbatchname" = "td" ]; then
        testbatch="005 006 007 025 046 049 052 056 059 072 091 097 100"
        testbatchtimeout=1200
      elif [ "$testbatchname" = "td+" ]; then
        testbatch="005 006 007 025 046 049 052 056 059 072 091 097 100 016 047 054 075 092"
        testbatchtimeout=1200
      else
        testbatch="$testbatchname"
      fi
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done  

if [ "$testbatchset" -eq "0" ]; then
  tests=`ls $testdir/*.graph`
else
  if [ "$timeoutset" -eq "0" ]; then
     instancetimeout=$testbatchtimeout
  fi
  if [ "$testdirset" -eq "0" ]; then
     testdir="pace16-fvs-instances"
  fi
  test_files=()
  for t in $testbatch; do
    test_files+=("$testdir/$t.graph")
  done
  tests=${test_files[@]} 
fi

totaltests=0
successtests=0
timetmp=`mktemp`

echo "TIME LIMIT = $instancetimeout"
if [ "$testbatchset" -eq "1" ]; then
  echo "TEST BATCH = $testbatchname"
fi
echo ""

for i in $tests
do
  echo -e -n "${mark_test}Test "
  printf "%30s" $i
  echo -e -n "${mark_end}:           "
  outfile=${i%.graph}.out
  /usr/bin/time -q --output=$timetmp -f "%E" timeout $instancetimeout $exe -f $i > $outfile
  ret=$?
  echo -n "(time "
  cat $timetmp | tr "\n" " "
  echo -n ")    "
  if (( $ret == 124 ))
  then
    echo -e "${mark_tle}TIMEOUT${mark_end}"
  elif (( $ret == 0 ))
  then
    cp $outfile $outfile-last
    optfile=${i%.graph}.opt
    if [ ! -f "$optfile" ]
    then
      optfile=""
    fi
    $ver $i $outfile $optfile
    retver=$?
    echo -n "        "
    if (( $retver == 0 ))
    then
      successtests=$(( $successtests + 1 ))
      echo -e "${mark_ok}OK${mark_end}"
    elif (( $retver == 2 ))
    then
      successtests=$(( $successtests + 1 ))
      echo -e "${mark_ok}OK (better than opt?)${mark_end}"
    elif (( $retver == 1 ))
    then
      echo -e "${mark_ok}FEASIBLE (worse than opt)${mark_end}"
    else
      echo -e "${mark_wrong}WRONG${mark_end}"
    fi
  else
    echo -e "${mark_wrong}RUNTIME ERROR${mark_end}"
  fi
  rm -f $outfile
  totaltests=$(( $totaltests + 1 ))
done

echo "-----------------------"
echo "Passed $successtests out of $totaltests"

