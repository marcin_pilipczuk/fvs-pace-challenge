import csv
import re

outputfile = "raports/resultscomp-last.csv"

test_line_re = re.compile("Test pace16-fvs-instances//?(?P<id>\\d\\d\\d).graph: *\\(time (?P<time>\\d+:\\d+\\.\\d+) \\) *(?P<result>.*)")
header_line_re = re.compile("(?P<name>[^=]*)=(?P<value>.*)")

files_prefix = "raports/"
files = [
  "results-2016-07-07-180753-aa6b024867bec4574ed9ef2571467b5d6e1e70de.txt",
  "results-2016-07-07-211722-f8a7d6114025c33453827137a777ac16527a14a3.txt",
  "results-2016-07-08-093016-e7dccaad05f6e712a99d905896bee07efc8784a3.txt",
  "results-2016-07-21-220229-3941afd659ffbdd50b2a1333c8822b6da320ade5.txt",
  "results-2016-07-23-202527-625c8180bdd1a84f8c059bd1d10c91a558f470a4.txt",
  "results-2016-07-27-202947-a225f265689be59bc84ac4a6c5766c4c1ef48695.txt",
  "results-2016-07-28-150416-b5aec2c9eb4452921d9d3d99a7ac56218460052c.txt",
  "results-2016-07-28-214402-8208f38809ab2aafa425a009417f387ea7125a8e.txt",
  "results-2016-07-29-172703-7f0da70045076352bf2982fe0936fc83d3dcf8a3.txt",
]
files2 = [
  "results-2016-07-21-213133-3941afd659ffbdd50b2a1333c8822b6da320ade5.txt",
  "results-2016-07-23-164938-749b7aa23cb0b849d4568d0b628232ce3d6678dd.txt",
  "results-2016-07-24-092722-6e78abcd684615940f60b2250376586391e7251b.txt",
  "results-2016-07-24-103241-6e78abcd684615940f60b2250376586391e7251b.txt",
  "results-2016-07-24-105909-6e78abcd684615940f60b2250376586391e7251b.txt",
  "results-2016-07-24-112301-6e78abcd684615940f60b2250376586391e7251b.txt",
]

tests = [("%03d" % x) for x in xrange(1, 101)]
tests2 = ['005', '009', '011', '037', '044', '046', '049', '092', '017', '076', '069', '051']
headers = {}

def read_result_file(filename):
  filename = "%s%s" % (files_prefix, filename)
  f = open(filename)
  still_header = True
  res = {}
  for line in f.readlines():
    mo = test_line_re.match(line)
    if mo:
      still_header = False
      if mo.group('result').find('OK') >= 0:
        res[mo.group('id')] = mo.group('time')
      elif mo.group('result').find('TIMEOUT') >= 0:
        res[mo.group('id')] = 'TLE'
      else:
        res[mo.group('id')] = 'ERR'
    elif still_header:
      mo = header_line_re.match(line)
      if mo:
        name = mo.group('name')
        value = mo.group('value')
        res[name] = value
        headers[name] = True
  return res


res = [read_result_file(f) for f in files]

outf = open(outputfile, "wb")
out = csv.writer(outf)
out.writerow(["Timestamp"] + [f[8:25] for f in files])
for h in headers.iterkeys():
  out.writerow([h] + [r.get(h, "") for r in res])
for t in tests:
  out.writerow([t] + [r.get(t, "") for r in res])
outf.close()  
